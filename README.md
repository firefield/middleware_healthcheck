# MiddlewareHealthcheck

```
__        ___    ____  _   _ ___ _   _  ____
\ \      / / \  |  _ \| \ | |_ _| \ | |/ ___|
 \ \ /\ / / _ \ | |_) |  \| || ||  \| | |  _
  \ V  V / ___ \|  _ <| |\  || || |\  | |_| |
   \_/\_/_/   \_\_| \_\_| \_|___|_| \_|\____|
```

This repository has been moved and will be deleted soon, if you use this code and you require it from the repository, change it to use the published gem instead, or just clone this repository to your own account.


## Installation
Add this line to your application's Gemfile:
```ruby
gem 'middleware_healthcheck'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install middleware_healthcheck
```

## Usage
Run basic check (without running any advanced checkers):
```
/healthcheck
```

To run all available checkers, add `full` parameter:
```
/healthcheck?full=1
```

To run selected checkers, add `checks` parameter, where the value is the name of checkers separated by a comma:
```
/healthcheck?checks=active_record,second_checker,another_checker
```

To change default path and parameters see Configuration section

## Configuration
Create ``config/initializers/middleware_healthcheck.rb`` file
```ruby
MiddlewareHealthcheck.configure do |config|
  config.healthcheck_path = "my_custom_path"
  ...
end
```
Available options:
```
healthcheck_path
full_check_param_name
selected_check_param_name
error_response_status
success_response_status
success_response_body
errors_delimiter
selected_check_param_split_delimiter
```

## Custom Checkers
Your Custom Checker class should look like this:
```ruby
class MyCustomChecker
  attr_accessor :error
 
  def initialize(_app, _env)
  end

  def healthy?
    if everything_ok?
      true
    else
      self.error = 'Error message'
      false
    end
  end
end
```

To include Custom Checker, just add
```ruby
HealthcheckMiddleware.configure do |config|
  config.checkers << MyCustomChecker
end
```
in initializer.

## Development

```
# build the docker containers
docker-compose build

# run the specs
docker-compose run --rm app bundle exec rspec

```

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
