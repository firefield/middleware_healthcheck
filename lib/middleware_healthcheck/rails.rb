module MiddlewareHealthcheck
  class Railtie < Rails::Railtie
    initializer "middleware_healthcheck.configure_rails_initialization" do
      app.middleware.insert_after Rails::Rack::Logger, MiddlewareHealthcheck::Middleware
    end

    def app
      Rails.application
    end
  end
end
