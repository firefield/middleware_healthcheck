module MiddlewareHealthcheck
  class Middleware
    PATH_INFO_KEY = "PATH_INFO".freeze

    def initialize(app)
      @app = app
    end

    def call(env)
      if env[PATH_INFO_KEY] == MiddlewareHealthcheck.configuration.healthcheck_path
        MainChecker.new(@app, env).check_health
      else
        @app.call(env)
      end
    end
  end
end
