require "middleware_healthcheck/rails" if defined? Rails::Railtie
require "middleware_healthcheck/default_checkers"
require "middleware_healthcheck/configuration"
require "middleware_healthcheck/main_checker"
require "middleware_healthcheck/middleware"

module MiddlewareHealthcheck
  class << self
    attr_accessor :configuration

    def configuration
      @configuration ||= Configuration.new
    end

    def configure
      yield configuration
    end
  end
end
